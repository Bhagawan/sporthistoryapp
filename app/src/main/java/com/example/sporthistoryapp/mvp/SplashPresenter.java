package com.example.sporthistoryapp.mvp;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.example.sporthistoryapp.data.SplashResponse;
import com.example.sporthistoryapp.util.MyServerClient;
import com.example.sporthistoryapp.util.ServerClient;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Locale;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashPresenterViewInterface> {
    private Target target;
    private boolean requestInProcess = false;

    public void requestSplash() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<SplashResponse> call = client.getSplash(Locale.getDefault().getLanguage());
        showLogo();
        call.enqueue(new Callback<SplashResponse>() {
            @Override
            public void onResponse(@NonNull Call<SplashResponse> call, @NonNull Response<SplashResponse> response) {
                if(requestInProcess) {
                    requestInProcess = false;
                    getViewState().hideLogo();
                }
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    String s = response.body().getUrl();
                    if(s == null) getViewState().switchToMain();
                    else if(s.equals("no")) {
                        getViewState().switchToMain();
                    }
                    else {
                        getViewState().showSplash("https://" + s);
                    }
                } else {
                    getViewState().showError("Server error 2");
                    getViewState().switchToMain();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SplashResponse> call, @NonNull Throwable t) {
                getViewState().showError("Server error 3");
                getViewState().switchToMain();
            }
        });
    }

    private void showLogo() {
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if(requestInProcess) getViewState().showLogo(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        requestInProcess = true;
        Picasso.get().load("http://195.201.125.8/SportHistoryApp/logo.png").into(target);
    }
}
