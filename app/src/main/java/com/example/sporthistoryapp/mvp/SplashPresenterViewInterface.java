package com.example.sporthistoryapp.mvp;

import android.graphics.Bitmap;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface SplashPresenterViewInterface extends MvpView {

    @SingleState
    void switchToMain();

    @OneExecution
    void showError(String error);

    @SingleState
    void showSplash(String url);

    @OneExecution
    void showLogo(Bitmap logo);

    @OneExecution
    void hideLogo();
}
