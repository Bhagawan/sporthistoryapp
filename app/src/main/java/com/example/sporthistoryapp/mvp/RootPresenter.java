package com.example.sporthistoryapp.mvp;

import androidx.annotation.NonNull;

import com.example.sporthistoryapp.data.Article;
import com.example.sporthistoryapp.util.MyServerClient;
import com.example.sporthistoryapp.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class RootPresenter extends MvpPresenter<RootPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        Call<List<Article>> call = MyServerClient.createService(ServerClient.class).getArticles();
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(@NonNull Call<List<Article>> call, @NonNull Response<List<Article>> response) {
                if(response.isSuccessful() && response.body() != null) {
                    getViewState().fillAdapter(response.body());
                } else getViewState().serverError();
            }

            @Override
            public void onFailure(@NonNull Call<List<Article>> call, @NonNull Throwable t) {
                getViewState().serverError();
            }
        });
    }
}
