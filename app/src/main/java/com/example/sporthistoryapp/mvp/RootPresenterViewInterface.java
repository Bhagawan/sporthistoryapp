package com.example.sporthistoryapp.mvp;

import com.example.sporthistoryapp.data.Article;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface RootPresenterViewInterface extends MvpView {

    @SingleState
    void fillAdapter(List<Article> articles);

    @OneExecution
    void serverError();
}
