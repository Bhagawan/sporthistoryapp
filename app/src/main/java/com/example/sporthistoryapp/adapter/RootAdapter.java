package com.example.sporthistoryapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sporthistoryapp.R;
import com.example.sporthistoryapp.data.Article;

import java.util.List;

public class RootAdapter extends RecyclerView.Adapter<RootAdapter.ViewHolder> {
    private List<Article> articles;
    private ItemClickListener mClickListener;

    public RootAdapter(List<Article> articles) {
        this.articles = articles;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_root, parent, false);
        return new RootAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.text.setText(articles.get(position).sport);
    }

    @Override
    public int getItemCount() {
        if(articles != null) return articles.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private TextView text;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public Article getArticle(String header) {
        for(Article article: articles) {
            if(article.sport.equals(header)) return article;
        }
        return null;
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
