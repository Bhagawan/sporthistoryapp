package com.example.sporthistoryapp.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sporthistoryapp.R;
import com.example.sporthistoryapp.data.Article;
import com.example.sporthistoryapp.data.TextElement;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ArticleFragment extends Fragment {
    private View mView;
    private Target target;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_article, container, false);
        setBackground();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Article a = (Article) bundle.getSerializable("article");
            if(a != null) createArticle(a);
        }

        return mView;
    }

    private void createArticle(Article article) {
        LinearLayout ll = mView.findViewById(R.id.layout_article);

        TextView header = new TextView(getContext());
        header.setText(article.header);
        header.setTextColor(Color.BLACK);
        header.setTextSize(30);
        header.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        ll.addView(header, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        for(TextElement element : article.history) {
            if(element.text.length() > 0) {
                TextView text = new TextView(getContext());
                text.setText(element.text);
                text.setTextColor(Color.BLACK);
                text.setTextSize(20);
                ll.addView(text, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
            if(element.img.length() > 0) {
                ImageView image = new ImageView(getContext());
                Picasso.get().load(element.img).into(image);
                ll.addView(image, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
        }
    }

    private void setBackground() {
        ConstraintLayout constraintLayout = mView.findViewById(R.id.layout_article_base);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                constraintLayout.setBackground(new BitmapDrawable(getResources(), bitmap));
            }

            @Override public void onBitmapFailed(Exception e, Drawable errorDrawable) { }

            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        };
        Picasso.get().load("http://195.201.125.8/SportHistoryApp/paper.png").into(target);
    }
}