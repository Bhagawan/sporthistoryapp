package com.example.sporthistoryapp.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.sporthistoryapp.MainActivity;
import com.example.sporthistoryapp.R;
import com.example.sporthistoryapp.adapter.RootAdapter;
import com.example.sporthistoryapp.data.Article;
import com.example.sporthistoryapp.mvp.RootPresenter;
import com.example.sporthistoryapp.mvp.RootPresenterViewInterface;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class RootFragment extends MvpAppCompatFragment implements RootPresenterViewInterface {
    private View mView;
    private Target target;

    @InjectPresenter
    RootPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_root, container, false);
        setBackground();

        ImageButton imageButton = mView.findViewById(R.id.image_toolbar_menu);
        imageButton.setOnClickListener(view -> {
            Activity a = requireActivity();
            if(a instanceof MainActivity) {
                ((MainActivity) a).openMenu();
            }
        });

        SearchView searchView = mView.findViewById(R.id.search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                findArticle(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return mView;
    }

    @Override
    public void fillAdapter(List<Article> articles) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_root);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RootAdapter adapter = new RootAdapter(articles);
        recyclerView.setAdapter(adapter);

        adapter.setClickListener((view, position) -> goToArticle(articles.get(position)));

    }

    @Override
    public void serverError() {
        Toast.makeText(getContext(), getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show();
    }

    private void findArticle(String header) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_root);
        RootAdapter adapter = (RootAdapter)recyclerView.getAdapter();
        if(adapter != null) {
            Article a = adapter.getArticle(header);
            if(a != null) {
                goToArticle(a);
            } else searchError();
        }

    }

    private void goToArticle(Article article) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("article", article);
        fragment.setArguments(bundle);

        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainerView, fragment)
                .addToBackStack("home")
                .commit();
    }

    private void searchError() {
        Toast.makeText(getContext(), getString(R.string.msg_error_search), Toast.LENGTH_SHORT).show();
    }

    private void setBackground() {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_root);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                recyclerView.setBackground(new BitmapDrawable(getResources(), bitmap));
            }

            @Override public void onBitmapFailed(Exception e, Drawable errorDrawable) { }

            @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
        };
        Picasso.get().load("http://195.201.125.8/SportHistoryApp/paper.png").into(target);
    }
}