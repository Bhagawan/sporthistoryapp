package com.example.sporthistoryapp.util;

import com.example.sporthistoryapp.data.Article;
import com.example.sporthistoryapp.data.SplashResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServerClient {

    @FormUrlEncoded
    @POST("SportHistoryApp/splash.php")
    Call<SplashResponse> getSplash(@Field("locale")String locale);

    @GET("SportHistoryApp/sport_history.json")
    Call<List<Article>> getArticles();

}
