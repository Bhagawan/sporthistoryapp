package com.example.sporthistoryapp;

import android.app.Application;

import com.onesignal.OneSignal;

public class SportHistoryApp extends Application {

    private static final String ONESIGNAL_APP_ID = "0f7a1f96-50aa-4b15-83a9-5baa1014d5d4";

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }
}
