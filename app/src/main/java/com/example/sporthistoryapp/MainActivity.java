package com.example.sporthistoryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.example.sporthistoryapp.fragments.RootFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fullscreen();
        DrawerLayout dl = findViewById(R.id.layout_drawer);
        View label = findViewById(R.id.menu_label);
        LinearLayout menu = findViewById(R.id.menu);

        ImageView homeButton = findViewById(R.id.image_menu_home);
        homeButton.setOnClickListener(view -> {
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 0) {
                for(int i = 0; i <  fm.getBackStackEntryCount(); i++) {
                    String name = fm.getBackStackEntryAt(i).getName();
                    if(name == null) name = "";
                    if(name.equals("home"))  fm.popBackStack("home", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            } else getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainerView, new RootFragment())
                        .commit();
        });


        ImageView exitButton = findViewById(R.id.image_menu_exit);
        exitButton.setOnClickListener(view -> finish());

        dl.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                ConstraintLayout.LayoutParams p = (ConstraintLayout.LayoutParams) label.getLayoutParams();
                p.setMarginStart((int) (menu.getWidth() * slideOffset));
                label.setLayoutParams(p);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void openMenu() {
        DrawerLayout dl = findViewById(R.id.layout_drawer);
        dl.open();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof SearchView) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            } else {
                WindowInsetsControllerCompat controllerCompat = new WindowInsetsControllerCompat(getWindow(), getWindow().getDecorView());
                controllerCompat.hide(WindowInsetsCompat.Type.ime());
            }
        }
        fullscreen();
        return super.dispatchTouchEvent( event );
    }


    private void fullscreen() {
        WindowInsetsControllerCompat controllerCompat = new WindowInsetsControllerCompat(getWindow(), getWindow().getDecorView());
        controllerCompat.hide(WindowInsetsCompat.Type.systemBars());
        controllerCompat.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);
    }
}