package com.example.sporthistoryapp.data;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Keep
public class TextElement  implements Serializable {
    @SerializedName("text")
    public String text;
    @SerializedName("img")
    public String img;
}
