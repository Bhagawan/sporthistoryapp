package com.example.sporthistoryapp.data;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Keep
public class Article implements Serializable {
    @SerializedName("sport")
    public String sport;
    @SerializedName("header")
    public String header;
    @SerializedName("history")
    public TextElement[] history;
}
