package com.example.sporthistoryapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;


import com.example.sporthistoryapp.mvp.SplashPresenter;
import com.example.sporthistoryapp.mvp.SplashPresenterViewInterface;
import com.example.sporthistoryapp.util.MyWebChromeClient;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends MvpAppCompatActivity implements SplashPresenterViewInterface {
    private WebView mWebView;

    @InjectPresenter
    SplashPresenter mPresenter;

    @Override
    public void onBackPressed() {
        if (mWebView!=null){
            if (mWebView.canGoBack()){
                mWebView.goBack();
                return;
            }
        } else finish();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mWebView = findViewById(R.id.webView_splash);
        initialiseWebView();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
        if(savedInstanceState == null) mPresenter.requestSplash();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void switchToMain() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getApplicationContext().startActivity(i);
        finish();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSplash(String url) {
        mWebView = findViewById(R.id.webView_splash);
        mWebView.loadUrl(url);
    }

    @Override
    public void showLogo(Bitmap logo) {
        mWebView = findViewById(R.id.webView_splash);
        ConstraintLayout logoLayout = findViewById(R.id.layout_splash_logo);
        ImageView imageLogo = findViewById(R.id.imageView_splash_logo);
        imageLogo.setImageBitmap(logo);
        mWebView.setVisibility(View.GONE);
        logoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLogo() {
        mWebView = findViewById(R.id.webView_splash);
        ConstraintLayout logoLayout = findViewById(R.id.layout_splash_logo);
        mWebView.setVisibility(View.VISIBLE);
        logoLayout.setVisibility(View.GONE);
    }

    private void fullscreen() {
        WindowInsetsControllerCompat controllerCompat = new WindowInsetsControllerCompat(getWindow(), getWindow().getDecorView());
        controllerCompat.hide(WindowInsetsCompat.Type.systemBars());
        controllerCompat.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private  void initialiseWebView() {
        FrameLayout frame = findViewById(R.id.webView_customView);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new MyWebChromeClient(this, mWebView, frame));

        WebSettings webSettings = mWebView.getSettings();

        mWebView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        CookieManager cookie = CookieManager.getInstance();
        cookie.setAcceptCookie(true);
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.supportZoom();
        webSettings.setAppCacheEnabled(true);

        webSettings.setUserAgentString(webSettings.getUserAgentString().replace("; wv",""));
    }
}
